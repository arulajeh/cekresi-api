const axios = require('axios').default;
const db = require('../models');
const log4js = require('log4js');
log4js.configure({
    appenders: { everything: { type: 'file', filename: 'logs.log' } },
    categories: { default: { appenders: ['everything'], level: 'ALL' } }
});
const logger = log4js.getLogger();

const phoneFormatter = (number) => {
    let formatted = number.replace(/\D/g, '');
    if (!formatted.startsWith('62')) {
        formatted = '62' + formatted.substr(1);
    }
    if (!formatted.endsWith('@c.us')) {
        formatted += '@c.us';
    }
    return formatted;
}
const apiKey = "c3e5b5738b01e49a6e335a6e4c34da91";
const dataGetter = async (url, method = "", body, headers) => {
    switch (method.toLowerCase()) {
        case 'get' || 'delete':
            return axios[method.toLowerCase()](url, { headers: headers });
        case 'post' || 'put':
            return axios[method.toLowerCase()](url, body, { headers: headers });
    }
}
const encryptor = (data) => {
    const base = btoa(data).split("").reverse().join("|");
    const result = btoa(`${base}:${optionalData()}`);
    return result;
}
const decryptor = (data) => {
    const b = atob(data);
    const c = b.substring(0, b.indexOf(":"));
    const decrypted = atob(c.split("|").reverse().join(""));
    return decrypted;
}
const optionalData = () => {
    const d = new Date();
    const angka = `${d.getUTCFullYear()}.${d.getUTCMonth()}.${d.getUTCDate()}.${d.getUTCHours()}.${d.getUTCMinutes()}.${d.getUTCSeconds()}.${d.getUTCMilliseconds()}`;
    const result = btoa(angka).split("").join(":");
    return result;
}
const btoa = (data) => {
    let buff = new Buffer.from(data);
    let base64data = buff.toString('base64');
    return base64data;
}
const atob = (data) => {
    let buff = new Buffer.from(data, 'base64');
    let text = buff.toString('ascii');
    return text;
}
const myResponse = (call, code, msg, data) => {
    return call.status(code).send({
        status: code == 200 ? true : false,
        msg: msg,
        data: data
    });
}
const findOne = async (table, condition, attributes) => {
    const data = await db[table].findOne({
        where: condition,
        attributes: attributes
    });
    return data;
}
const findAll = async (table, condition, attributes) => {
    const data = await db[table].findAll({
        where: condition,
        attributes: attributes
    });
    return data
}
const getAllowedHost = async () => {
    let result = [];
    return findAll('m_cors', {status: 1}, ["host"]).then((val) => {
        result = val;
        return result;
    }).catch((err) => {
        console.error(err);
        result = [];
        return result;
    });
}
module.exports = {
    dataGetter,
    encryptor,
    decryptor,
    btoa,
    atob,
    myResponse,
    findOne,
    findAll,
    phoneFormatter,
    apiKey,
    getAllowedHost,
    logger
}
var express = require('express');
var api = express.Router();
const common = require('../common/common_function');
const rajaOngkirHost = "https://pro.rajaongkir.com";

api.get('/', (req, res) => {
    res.json({
        msg: "Hello worlds"
    });
});

api.get('/test', (req, res) => {
    res.json({
        msg: "HELLO WORLDS"
    });
});

api.get('/check-db', async (req, res) => {
    const data = await common.findOne('m_cors', {status: 1}, ['host', 'description']).then((val) => {
        return val;
    }).catch((err) => {
        return false;
    });
    console.log(data);
    res.json({
        msg: "HELLO WORLDS",
        data: data
    });
});

api.post('/app_detail', async (req, res) => {
    const headers = req.headers;
    const body = req.body;
    const package = body.id ? common.decryptor(body.id) : "";
    const attributes = ['package_name', 'app_name', 'version_code', 'version_name', 'store_url'];
    common.findOne('app_info', { package_name: package }, attributes).then((ress) => {
        if (ress) {
            common.myResponse(res, 200, "Get data successfully", ress);
        } else {
            common.myResponse(res, 400, "Failed get data", null);
        }
    }).catch((err) => {
        common.myResponse(res, 400, "Failed get data", err);
    });
});

const listUrl = [
    '/province', 
    '/city', 
    '/subdistrict', 
    '/v2/internationalOrigin', 
    '/v2/internationalDestination'
];
api.get(listUrl, async (req, res) => {
    const url = req.originalUrl;
    const ongkir = await common.findOne('m_key', {name: 'rajaongkir'}, ['key']);
    const headers = { key: ongkir.key };
    common.dataGetter(rajaOngkirHost + url, "GET", null, headers).then((val) => {
        common.myResponse(res, 200, "Get data successfully", val.data.rajaongkir.results);
    }).catch((err) => {
        console.log(err);
            let code = 400;
            let msg = "Failed get data";
        if (err.response.status == 400) {
            code = err.response.data.rajaongkir.status.code;
            msg = err.response.data.rajaongkir.status.description;
        };
        common.myResponse(res, code, msg);
    });
});

api.post('/cost', async (req, res) => {
    const body = req.body;
    let status = true;
    let message = "";
    let data;
    let payload = {
        'origin': body.origin,
        'originType': body.originType,
        'destination' : body.destination,
        'destinationType': body.destinationType,
        'weight': body.weight,
        'courier': body.courier
    };
    if (!payload.origin || !payload.originType || !payload.destination || !payload.destinationType || !payload.weight || !payload.courier) {
        status = false;
        message = "Fill all payload";
    } else {
        const ongkir = await common.findOne('m_key', {name: 'rajaongkir'}, ['key']);
        const headers = {key: ongkir.key};
        data = await common.dataGetter(rajaOngkirHost+'/api/cost', "POST", payload, headers)
        .then((resp) => {
            status = true;
            message = "Get data successfully";
            return resp.data.rajaongkir;
        }).catch((err) => {
            const code = err.response.status;
            status = false;
            message = code == 400 ? err.response.data.rajaongkir.status.description : "Failed get data";
            return false;
        });
    }
    common.myResponse(res, status ? 200 : 400, message, data);
});

api.post('/waybill', async (req, res) => {
    const body = req.body;
    const payload = {
        "waybill": body.waybill,
        "courier": body.courier
    };
    let status = true;
    let message = "";
    let data;
    if (!payload.waybill || !payload.courier) {
        status = false;
        message = "Fill all payload";
    } else {
        const ongkir = await common.findOne('m_key', {name: 'rajaongkir'}, ['key']);
        const headers = {key: ongkir.key};
        data = await common.dataGetter(rajaOngkirHost+'/api/waybill', req.method, payload, headers)
        .then((resp) => {
            status = true;
            message = "Get data successfully";
            return resp.data.rajaongkir;
        }).catch((err) => {
            const code = err.response.status;
            status = false;
            message = code == 400 ? err.response.data.rajaongkir.status.description : "Failed get data";
            return false;
        })
    }
    common.myResponse(res, status ? 200 : 400, message, data);
});

api.get('/courier-local-resi', (req, res) => {
    res.json({
        msg: "Oke"
    });
});

api.post('/waybill-log', (req, res) => {
    res.json({
        msg: "OKE"
    });
});

module.exports = api;
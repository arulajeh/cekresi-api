const express = require('express');
const app = express();
const cors = require('cors');
const compression = require('compression');
const api = require('./generalapp/route');
const ip = require('ip');
const common = require('./common/common_function');
const https = require('https');
const fs = require('fs');
const privateKey = fs.readFileSync('/etc/letsencrypt/live/ayungstore.my.id/privkey.pem', 'utf8');
const certificate = fs.readFileSync('/etc/letsencrypt/live/ayungstore.my.id/cert.pem', 'utf8');
const ca = fs.readFileSync('/etc/letsencrypt/live/ayungstore.my.id/chain.pem', 'utf8');

const credentials = {
	key: privateKey,
	cert: certificate,
	ca: ca
};

const corsOptionsDelegate = async function (req, callback) {
    var corsOptions;
    let host = await common.getAllowedHost();
    host = JSON.parse(JSON.stringify(host, null, 2));
    if (host.findIndex(x => x.host == req.header('Origin')) !== -1) {
        corsOptions = { origin: true };
    } else {
        corsOptions = { origin: false };
    };
    callback(null, corsOptions);
};
app.use(cors(corsOptionsDelegate));
app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(compression());
app.use((req, res, next) => {
    if (req.headers.authorization !== "IvEy5xqbVqI525KsdavQXbYZt83GJEH1" && req.path.startsWith('/api')) {
        res.status(401).send({
            status: false,
            messages: "Unauthorized"
        });
    } else {
        next();
    }
});
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header('Access-Control-Allow-Methods', 'DELETE, PUT');
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, X-Token");
    next();
});

app.get('/', (req, res) => {
    res.json({
        msg: "Hello Worlds"
    });
});
app.use('/api', api);

app.get('/api/check-ip', (req, res) => {
    res.json({
        ip: ip.address('public', 'ipv4')
    });
});

app.get('/get-log', (req, res) => {
    res.sendFile(__dirname + '/logs.log');
});

app.all('*', (req, res) => {
    res.status(404).sendFile(__dirname + '/views/notfound.html');
});

app.set('port', 80);
app.listen(app.get('port'), () => {
    console.log("Server running on port " + app.get('port'));
});
https.createServer(credentials, app).listen(443);
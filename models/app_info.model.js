module.exports = (sequelize, DataTypes) => {
  const App_info = sequelize.define('app_info', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    package_name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    app_name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    version_code: {
      type: DataTypes.INTEGER,
      allowNull: true,
    },
    version_name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    ads_network_id: {
      type: DataTypes.INTEGER,
      allowNull: true,
    },
    store_url: {
      type: DataTypes.STRING,
      allowNull: false,
    }
  },
  {
    freezeTableName: true
  });
  return App_info;
}

'use strict';

const fs = require('fs');
const path = require('path');
const {Sequelize, DataTypes} = require('sequelize');
//const Sequelize = require('sequelize-views-support');
const basename = path.basename(__filename);
// const env = process.env.NODE_ENV || 'development';
const env = 'ali';
const config = require(__dirname + '/../config/config.json')[env];
//config = require('../config/config.json')[env];
const db = {};

let sequelize;
//if (env === 'development') {
  sequelize = new Sequelize(config);
//} else {
  //sequelize = new Sequelize(config.database, config.username, config.password, config);
//  sequelize = new Sequelize(process.env.DATABASE_URL);
//}


fs
  .readdirSync(__dirname)
  .filter(file => {
    return (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js');
  })
  .forEach(file => {
    // const model = sequelize['import'](path.join(__dirname, file));
    const model = require(path.join(__dirname, file))(sequelize, DataTypes);
    db[model.name] = model;
  });
Object.keys(db).forEach(modelName => {
  if (db[modelName].associate) {
    db[modelName].associate(db);
  }
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;
console.log(env);
module.exports = db;
